import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  products: [],
  status: null,
};
export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async () => {
    const data = await fetch("/products.json");
    const responceData = await data.json();
    return responceData;
  }
);
const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = "pending";
      })
      .addCase(fetchProducts.rejected, (state) => {
        state.status = "error";
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = "done";
        state.products = action.payload;
      });
  },
});

export default productsSlice.reducer;
