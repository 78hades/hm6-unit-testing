import "@testing-library/jest-dom";
import productsReducer, { fetchProducts } from "./products.slice";

describe("products redux", () => {
  test("should fulfilled array", () => {
    const action = {
      type: fetchProducts.fulfilled.type,
      payload: [
        { id: "3", name: "Bill33", price: "$33", type: 'mouse' },
        { id: "4", name: "Bill33", price: "$120", type: 'keyboard' },
      ],
    };
    const result = productsReducer(undefined, action);
    expect(result.products).toEqual([
      { id: "3", name: "Bill33", price: "$33", type: 'mouse' },
      { id: "4", name: "Bill33", price: "$120", type: 'keyboard' },
    ]);
    expect(result.status).toEqual('done');
  });
});