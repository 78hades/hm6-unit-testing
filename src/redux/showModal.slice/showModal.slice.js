import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isShowModalFirst: false,
  isShowModalSecond: false,
};

const modalSLice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    isShowFirst: (state, action) => {
      state.isShowModalFirst = action.payload;
    },
    isShowSecond: (state, action) => {
      state.isShowModalSecond = action.payload;
    },
  },
});
export const { isShowFirst, isShowSecond } = modalSLice.actions;
export default modalSLice.reducer;
