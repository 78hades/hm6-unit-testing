import "@testing-library/jest-dom";

import modalReducer, {isShowFirst, isShowSecond } from './showModal.slice'

describe('modal redux', () => {
    test("should return default state when passed an empty action", () => {
      const result = modalReducer(undefined, { type: "" });
      expect(result).toEqual({   isShowModalFirst: false,
        isShowModalSecond: false, });
    });
  
    test('first modal', () => {
      const action = { type: isShowFirst.type, payload: true };
      const result = modalReducer(undefined, action);
      expect(result).toEqual({   isShowModalFirst: true,
        isShowModalSecond: false, });
    });
    test('second modal', () => {
        const action = { type: isShowSecond.type, payload: true };
        const result = modalReducer(undefined, action);
        expect(result).toEqual({   isShowModalFirst: false,
          isShowModalSecond: true, });
      });
  });
