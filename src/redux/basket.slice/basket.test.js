import "@testing-library/jest-dom";
import basketReducer, {
  fillBasket,
  clearBasket,
  lSCountBasket,
  lSbasket,
  lSGrandTotal,
  dataReset
} from "./basket.slice";

describe("basket redux", () => {
  test("should return default state when passed an empty action", () => {
    const result = basketReducer(undefined, { type: "" });
    expect(result).toEqual({
      basket: [],
      countBasket: 0,
      grandTotal: 0,
      isBasket: false,
    });
  });
  test("should add new product to basket", () => {
    const action = {
      type: fillBasket.type,
      payload: { id: "1", name: "mouse", price: "$33" },
    };
    const result = basketReducer(undefined, action);
    expect(result.basket).toHaveLength(1);
    expect(result.basket[0]).toEqual({
      id: "1",
      name: "mouse",
      price: "$33",
      quantity: 1,
      subtotal: "33.00",
    });
    expect(result.countBasket).toEqual(1);
    expect(result.grandTotal).toEqual("33.00");
  });
  test("should remove product from basket", () => {
    const initialState = {
        countBasket: 2,
        basket: [
            { id: "1", name: "mouse", price: "$33", quantity: 2, subtotal: "66.00" },
            { id: "2", name: "keyboard", price: "$22", quantity: 1, subtotal: "22.00" }
        ],
        isBasket: true,
        grandTotal: '88'
    };

    const action = {
        type: clearBasket.type,
        payload: { id: "2", name: "keyboard", price: "$22" },
    };

    const result = basketReducer(initialState, action);
    expect(result.basket).toHaveLength(1);
    expect(result.basket[0]).toEqual({
        id: "1",
        name: "mouse",
        price: "$33",
        quantity: 2,
        subtotal: "66.00",
    });
    expect(result.grandTotal).toEqual('66.00');
    expect(result.countBasket).toEqual(1);
    expect(result.isBasket).toEqual(true);
});
test('should clear basket', ()=>{
    const initialState = {
        countBasket: 2,
        basket: [
            { id: "1", name: "mouse", price: "$33", quantity: 2, subtotal: "66.00" },
            { id: "2", name: "keyboard", price: "$22", quantity: 1, subtotal: "22.00" }
        ],
        isBasket: true,
        grandTotal: '88'
    };
    const action = { type: dataReset.type}
    const result = basketReducer(initialState, action)
    expect(result.basket).toHaveLength(0)
    expect(result.basket).toEqual([])
    expect(result.countBasket).toEqual(0)
    expect(result.grandTotal).toEqual(0)
})
test('should save array od products in localStorage', ()=>{
    const action = {
        type: lSbasket.type,
        payload: { id: "2", name: "keyboard", price: "$22", subtotal:'22' },
    };
    const result = basketReducer(undefined, action)
    expect(result.basket).toEqual({id: "2", name: "keyboard", price: "$22", subtotal:'22'})
});
test('should save countBasket in localStorage', ()=>{
    const action = {
        type: lSCountBasket.type,
        payload: 8,
    };
    const result = basketReducer(undefined, action)
    expect(result.countBasket).toEqual(8)
});
test('should save grandTotal in localStorage', ()=>{
    const action = {
        type: lSGrandTotal.type,
        payload: 55,
    };
    const result = basketReducer(undefined, action)
    expect(result.grandTotal).toEqual(55)
});
});
