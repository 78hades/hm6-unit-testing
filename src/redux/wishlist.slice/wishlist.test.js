import "@testing-library/jest-dom";
import wishlistReducer, {
  fillWishlist,
  clearWishlist,
  lSWishlist,
  lSCountWishlist,
} from "./wishlist.slice";
describe("redux wishlist", () => {
  test("should return default state when passed an empty action", () => {
    const result = wishlistReducer(undefined, { type: "" });
    expect(result).toEqual({
      wishlist: [],
      countWishlist: 0,
      isWishlist: false,
    });
  });
  test("should add product to wishlist", () => {
    const initialState = {
      wishlist: [
        { id: "2", name: "keyboard", price: "$66" },
        { id: "5", name: "laptop", price: "$250" },
      ],
    };
    const action = { type: fillWishlist.type, payload: { id: "1", name: "mouse", price: "$33" } };
    const result = wishlistReducer(initialState, action);
    expect(result.wishlist).toEqual([
      { id: "2", name: "keyboard", price: "$66" },
      { id: "5", name: "laptop", price: "$250" },
      { id: "1", name: "mouse", price: "$33" },
    ]);
  });
  test("should remove product from wishlist", () => {
    const initialState = {
      wishlist: [
        { id: "2", name: "keyboard", price: "$66" },
        { id: "5", name: "laptop", price: "$250" },
        { id: "1", name: "mouse", price: "$33" } 
      ],
    };
    const action = { type: clearWishlist.type, payload: { id: "1", name: "mouse", price: "$33" } };
    const result = wishlistReducer(initialState, action);
    expect(result.wishlist).toEqual([
      { id: "2", name: "keyboard", price: "$66" },
      { id: "5", name: "laptop", price: "$250" },
    ]);
  });
  test("should save wishlist", () => {
    const action = { type: lSWishlist.type, payload: { id: "1", name: "mouse", price: "$33" },};
    const result = wishlistReducer(undefined, action);
    expect(result.wishlist).toEqual({ id: "1", name: "mouse", price: "$33" });
  });
  test("should save countWishlist in localStorage", () => {
    const action = { type: lSCountWishlist.type, payload: 33};
    const result = wishlistReducer(undefined, action);
    expect(result.countWishlist).toEqual(33);
  });
  });

