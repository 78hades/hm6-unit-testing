import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "./products.slice/products.slice";
import modalReducer from "./showModal.slice/showModal.slice";
import productReducer from "./product.slice/product.slice";
import basketReducer from "./basket.slice/basket.slice";
import wishlistReducer from './wishlist.slice/wishlist.slice'
export const store = configureStore({
  reducer: {
    products: productsReducer,
    modal: modalReducer,
    product: productReducer,
    basket: basketReducer,
    wishlist: wishlistReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});
