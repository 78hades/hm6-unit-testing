import "@testing-library/jest-dom";

import productReducer, {getProduct} from './product.slice'

describe('product slice', () => {
    test("should return default state when passed an empty action", () => {
      const result = productReducer(undefined, { type: "" });
      expect(result).toEqual({ product: {} });
    });
  
    test('get product', () => {
      const initialState = { product: {} };
      const action = { type: getProduct.type, payload: { id: "1", name: "mouse", price: "$33" } };
      const result = productReducer(initialState, action);
      expect(result).toEqual({ product: { id: "1", name: "mouse", price: "$33" } });
    });
  });
