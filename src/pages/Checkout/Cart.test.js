import React from "react";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Cart from "./Cart";

describe("product in wishlist", () => {
  const mockStore = configureStore([]);
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  test("render product", () => {
    const product = {
      name: "Bill33",
      color: "color",
      price: 10,
      subtotal: 40,
      quantity: 4,
      pathImg: "/path/to/image.png",
    };

    const component = renderer.create(
      <Provider store={store}>
        <Cart product={product} />
      </Provider>
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
