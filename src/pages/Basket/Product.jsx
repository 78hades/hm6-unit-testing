import Button from "../../components/Button/Button";
import deleteImg from './deletecon.png'
import { isShowFirst } from "../../redux/showModal.slice/showModal.slice";
import { useDispatch } from "react-redux";
import { getProduct } from "../../redux/product.slice/product.slice";
import { isBasketAction } from "../../redux/basket.slice/basket.slice";
import { memo } from "react";
function BasketProduct({ product }) {
  const dispatch = useDispatch();
  return (
    <>
      <tr className="cart-page__product product">
        <td className="first-col">
          <img className="product__img" src={product.pathImg} alt="" />
          <div className="product__about">
            <p className="product__about-name">{product.name}</p>
            <p className="product__about-color">Color: {product.color} </p>
            <p className="product__about-quantity">
              Quantity: {product.quantity}{" "}
            </p>
          </div>
        </td>
        <td className="product__price">{product.price}</td>
        <td className="product__shipping">Free</td>
        <td className="product__subtotal">${product.subtotal}</td>
        <td className="product__delete">
          <Button
            type="button"
            onClick={() => {
              dispatch(getProduct(product));
              dispatch(isShowFirst(true));
              dispatch(isBasketAction(true));
            }}
          >
            <img src={deleteImg} alt="" />
          </Button>
        </td>
      </tr>
    </>
  );
}

export default memo(BasketProduct);
