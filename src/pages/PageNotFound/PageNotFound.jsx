import { Link } from "react-router-dom"
import Cat from "./Без_названия-removebg-preview.png"
export default function PageNotFound(){
    return(
<main className="PageNotFound">
<h1 className="PageNotFound__404">404</h1>
<img className="PageNotFound__img" src={Cat} alt="" />
<h2 className="PageNotFound__Ooops">Ooops!</h2>
<p className="PageNotFound__title">Page Not Found</p>
<Link to='/'><button className="PageNotFound__button">GO BACK</button></Link>
</main>
    )
}