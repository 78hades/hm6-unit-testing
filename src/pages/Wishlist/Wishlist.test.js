import React from "react";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Product from "./Product";

describe("product in wishlist", () => {
  const mockStore = configureStore([]);
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  test("render product", () => {
    const product = {
      name: "Bill33",
      type: "mouse",
      color: "color",
      price: 10,
      pathImg: "/path/to/image.png",
    };

    const component = renderer.create(
      <Provider store={store}>
        <Product product={product} />
      </Provider>
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
