import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Modal from './Modal';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const mockStore = configureStore([]);

describe('Modal component', () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      modal: {
        isShowModalFirst: true,
        isShowModalSecond: false,
      },
      product: {
        product: {
          pathImg: 'path/to/image',
          name: 'Bill33',
          type: 'mouse',
          price: 100,
          color: 'black',
        },
      },
      basket: {
        isBasket: true,
      },
      wishlist: {
        isWishlist: false,
      },
    });
  });

  test('renders ModalWrapper, isShowModalFirst', () => {
    const { getByText } = render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
    expect(getByText('Product Delete!')).not.toBeNull();

    expect(getByText('*By clicking the “Yes, Delete” button, Bill33 will be deleted.')).not.toBeNull();
  });

  test('renders ModalWrapper, isShowSecondFirst', () => {
    store = mockStore({
      modal: {
        isShowModalFirst: false,
        isShowModalSecond: true,
      },
      product: {
        product: {
          pathImg: 'path/to/image',
          name: 'Bill33',
          type: 'mouse',
          price: '$33',
          color: 'black',
        },
      },
      basket: {
        isBasket: false,
      },
      wishlist: {
        isWishlist: false,
      },
    });

    const { getByText } = render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
    expect(getByText('ADD TO CART')).not.toBeNull();
  });

  test('dispatches correct actions when buttons are clicked', () => {
    const { getByText } = render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );

    fireEvent.click(getByText('YES, DELETE'));
    const actions = store.getActions();
    expect(actions[0].type).toEqual('basket/clearBasket');
    expect(actions[1].type).toEqual('basket/isBasketAction');
  });
});
