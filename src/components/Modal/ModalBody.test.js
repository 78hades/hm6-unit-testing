import React from "react";
import renderer from "react-test-renderer";
import ModalBody from "./ModalBody";

describe("ModalBody", () => {
  test("render ModalBody", () => {
    const component = renderer.create(
      <ModalBody type="Type" price="Price" color="Color">
        Content
      </ModalBody>
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
