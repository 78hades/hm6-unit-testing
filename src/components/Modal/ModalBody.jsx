export default function ModalBody({ type, price, color, children }) {
  return (
    <>  <span className="products__product-type">{type}</span>
      <span className="products__product-price">{price}</span>
      <span className="products__product-color">{color}</span>
      <p className="modal__content-comment">{children}</p>
    </>
  );
}
