import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ModalFooter from "./ModalFooter";

describe("ModalFooter component", () => {
  test("render button and check handle click", () => {
    const firstText = "NO, CANCEL";
    const secondaryText = "YES, DELETE";
    const firstClickMock = jest.fn();
    const secondaryClickMock = jest.fn();
    const { getByText } = render(
      <ModalFooter
        firstText={firstText}
        secondaryText={secondaryText}
        firstClick={firstClickMock}
        secondaryClick={secondaryClickMock}
      />
    );
    const firstButton = getByText(firstText);
    const secondaryButton = getByText(secondaryText);
    expect(firstButton).toBeTruthy();
    expect(secondaryButton).toBeTruthy();
    fireEvent.click(firstButton);
    fireEvent.click(secondaryButton);
    expect(firstClickMock).toHaveBeenCalled();
    expect(secondaryClickMock).toHaveBeenCalled();
  });
});
