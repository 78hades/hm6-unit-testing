import CartProduct from "./CartProduct";
import React, { useState } from "react";
import Modal from "../Modal/Modal";
import { fetchProducts } from "../../redux/products.slice/products.slice";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import TableCartProduct from "./tableCartProduct";
export default function ListProducts() {
  const dispatch = useDispatch();
  const { products, status } = useSelector((state) => state.products);
  const ListStructureContext = React.createContext();
  const [listStructure, setListStructure] = useState("cart");

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  useEffect(() => {
    const newValue = localStorage.getItem("list");
    if (newValue) {
      setListStructure(JSON.parse(newValue))
    }
  }, [setListStructure]);

  useEffect(() => {
if(listStructure !== 'cart'){
      localStorage.setItem("list", JSON.stringify(listStructure));
}
  }, [listStructure]);
  if (status === "error") {
    return <h1>No Products</h1>;
  }

  const cartProduct = products.map((product) => (
    <CartProduct key={product.id} product={product}></CartProduct>
  ));

  const tableCartProduct = products.map((product) => (
    <TableCartProduct key={product.id} product={product}></TableCartProduct>
  ));
  return (
    <>
      <ListStructureContext.Provider value={listStructure}>
        <h3 className="products__title">Product</h3>
        <button
          className="products__list-structure"
          onClick={() => {
            setListStructure(listStructure === "cart" ? "table" : "cart");
            console.log(listStructure);
          }}
        >
          {" "}
          <ListStructureContext.Consumer>
            {(value) =>
              value === "cart" ? (
                <>
                  <div className="products__list-structure-cart"></div>
                  <div className="products__list-structure-cart"></div>
                  <div className="products__list-structure-cart"></div>
                  <div className="products__list-structure-cart"></div>
                </>
              ) : (
                <>
                  <div className="products__list-structure-table"></div>
                  <div className="products__list-structure-table"></div>
                </>
              )
            }
          </ListStructureContext.Consumer>
        </button>{" "}
        <ListStructureContext.Consumer>
          {(value) =>
            value === "cart" ? (
              <>
                <section className="products">{cartProduct}</section>
              </>
            ) : (
              <table className="table__products">
                <tbody>
                  <tr className="table__products-details">
                    <td>products details</td>
                    <td>price</td>
                    <td>Add to cart</td>
                    <td>Add to wishlist</td>
                  </tr>
                  {tableCartProduct}
                </tbody>
              </table>
            )
          }
        </ListStructureContext.Consumer>
        <Modal></Modal>
      </ListStructureContext.Provider>
    </>
  );
}
