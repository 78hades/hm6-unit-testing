import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Button from './Button';

test('Button click event', () => {
  const onClickMock = jest.fn();

  render(
    <Button type="button" onClick={onClickMock}>
      Add to cart
    </Button>
  );
  fireEvent.click(screen.getByText('Add to cart'));
  expect(onClickMock).toHaveBeenCalled();
});