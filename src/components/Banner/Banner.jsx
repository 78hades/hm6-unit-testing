import Stall from "./Vector (3).png";
import VR from "./unsplash_Zf0mPf4lG-U.png";
import Stars from "./Group 14 (1).png";
export default function Banner() {
  return (
    <section className="banner">
      <aside className="banner__about">
        <img className="banner__stars" src={Stars} alt="" />
        <h1 className="banner__title">
          [Oculus <span className="banner__title-VR">VR</span>]
        </h1>
        <h2 className="banner__subtitle">Virtual Reality</h2>
        <p className="banner__text">
          VR box 360 originar complete <br></br>game. VR gaming complete set of
          2 <br></br>remotes.
        </p>
        <div className="banner__btn">
          <img className="banner__btn-img" src={Stall} alt="" />
          <button className="banner__btn-text">Order Now</button>
        </div>
      </aside>
      <img className="banner__VR-img" src={VR} alt="" />
    </section>
  );
}
