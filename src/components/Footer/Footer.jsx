import Facebook from "./Group 10.png";
import Instagram from "./Group 11.png";
import Twitter from "./Group 12.png";
import Linkedin from "./Group 13.png";
export default function Footer() {
  return (
    <section className="footer">
      <div className="footer__info">
        <h3>Need Help</h3>
        <p>Contact Us</p>
        <p>Track Order</p>
        <p>Returnds & Refunds</p>
        <p>FAQ's</p>
        <p>Career</p>
      </div>
      <div className="footer__info">
        <h3>Company</h3>
        <p>About Us</p>
        <p>gamestore BLOG</p>
        <p>gamestorestan</p>
        <p>Collabaration</p>
        <p>Media</p>
      </div>
      <div className="footer__info">
        <h3>More info</h3>
        <p>Term and Conditions</p>
        <p>Privacy Policy</p>
        <p>Shipping Policy</p>
        <p>Sitemap</p>
      </div>
      <div className="footer__info">
        <h3>Location</h3>
        <p>support@gamestore.in</p>
        <p>Eklingpura Chauraha, Ahmedabad Main Road</p>
        <p>(NH 8 - Near Mahadev Hotels) Udaipur, India - 313002</p>
      </div>
      <div className="footer__socials">
        <img src={Facebook} alt="" />
        <img src={Instagram} alt="" />
        <img src={Twitter} alt="" />
        <img src={Linkedin} alt="" />
      </div>
      <span>Copyrigth 2023 Gamestore Folks Pvt Ltd. All rigths reserved.</span>
    </section>
  );
}
